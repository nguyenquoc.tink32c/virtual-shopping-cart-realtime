// Login type (1: Shop, 2: Customer)
export const LOGIN_TYPE_SHOP = "1";
export const LOGIN_TYPE_CUSTOMER = "2";

// Status deliver
export const STATUS_CONFIRMED = "Confirmed";
export const STATUS_SEND_TO_KITCHEN = "SendToKitchen";
export const STATUS_READY_TO_DELIVERY = "ReadyToDeliver";
export const STATUS_READY_TO_PICKUP = "ReadyToPickup";
export const STATUS_COMPLETED = "Completed";
export const STATUS_CANCELLED = "Cancelled";

// Storage Key
export const STORAGE_KEY_LOGIN_SHOP_ID = "shopId";
export const STORAGE_KEY_LOGIN_CUSTOMER_ID = "customerId";
export const STORAGE_KEY_LOGIN_CUSTOMER_NAME = "customerName";
export const STORAGE_KEY_LOGIN_TYPE = "loginType";
export const STORAGE_KEY_CART_ID = "cartId";
