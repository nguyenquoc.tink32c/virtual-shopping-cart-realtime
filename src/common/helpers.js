import { trim } from "lodash";
import Moment from "react-moment";
import {
  STATUS_CONFIRMED,
  STATUS_SEND_TO_KITCHEN,
  STATUS_READY_TO_DELIVERY,
  STATUS_READY_TO_PICKUP,
  STATUS_COMPLETED,
  STATUS_CANCELLED,
} from "./constants";

const helpers = {
  /**
   * Format currency
   * Ex: 1.25 -> $1.25
   *
   */
  formatCurrency: function (numStr) {
    return "$" + Number(numStr.toFixed(2)).toLocaleString() + " ";
  },
  /**
   * Format Id
   * Ex: 123456 -> #123456
   *
   */
  formatId: function (str) {
    return "#" + str + " ";
  },
  /**
   * Format DateTime
   * Ex: 2021-10-22T14:36:48.9941931 -> 14:36:48 Oct 22nd, 2021
   *
   */
  formatDateTime: function (str) {
    return <Moment format="HH:mm:ss MMM Do, YYYY">{str}</Moment>;
  },
  /**
   * Get status name
   * Ex : 0 -> Confirmed, 1 -> Send to kitchen, ...
   */
  getStatusName: function (status) {
    let statusName = "";
    switch (trim(status)) {
      case STATUS_CONFIRMED:
        statusName = "Confirmed";
        break;
      case STATUS_SEND_TO_KITCHEN:
        statusName = "Send to kitchen";
        break;
      case STATUS_READY_TO_DELIVERY:
        statusName = "Ready to delivery";
        break;
      case STATUS_READY_TO_PICKUP:
        statusName = "Ready to pickup";
        break;
      case STATUS_COMPLETED:
        statusName = "Completed";
        break;
      case STATUS_CANCELLED:
        statusName = "Cancelled";
        break;
      default:
        statusName = "";
        break;
    }

    return statusName;
  },
};

export default helpers;
