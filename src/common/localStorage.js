const localStorage = {
  setStorage: function (key, value) {
    window.localStorage.setItem(key, value);
  },
  getStorage: function (key) {
    return window.localStorage.getItem(key);
  },
  removeStorage: function (key) {
    window.localStorage.removeItem(key);
  },
  clearStorage: function () {
    window.localStorage.clear();
  },
};

export default localStorage;
