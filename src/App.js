import React, { Suspense } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";

import LoadingSpinner from "./components/UI/LoadingSpinner";
import Layout from "./components/layout/Layout";

const ShopLogin = React.lazy(() => import("./components/login/ShopLogin"));
const ShopRegister = React.lazy(() => import("./components/shop/info/NewShop"));
const CustomerLogin = React.lazy(() =>
  import("./components/login/CustomerLogin")
);
const ShopMain = React.lazy(() => import("./components/shop/Index"));
const CustomerMain = React.lazy(() =>
  import("./components/customer/index/Index")
);
const OrderStatus = React.lazy(() =>
  import("./components/customer/orderStatus/OrderStatus")
);
const CustomerOrderList = React.lazy(() =>
  import("./components/customer/index/OrderList")
);
const CustomerHost = React.lazy(() =>
  import("./components/customer/index/IndexNotCart")
);
const OrdersList = React.lazy(() =>
  import("./components/shop/order/OrdersList")
);
const OrderDetail = React.lazy(() =>
  import("./components/shop/order/OrderDetail")
);
const AddEditItem = React.lazy(() =>
  import("./components/shop/item/AddEditItem")
);
const ShopInfo = React.lazy(() => import("./components/shop/info/ShopInfo"));

function App() {
  return (
    <BrowserRouter>
      <Suspense
        fallback={
          <div className="centered">
            <LoadingSpinner />
          </div>
        }
      >
        <Switch>
          <Route exact path="/shop/login" component={ShopLogin} />
          <Route exact path="/shop/regist" component={ShopRegister} />
          <Route exact path="/host/shop/:shopId" component={CustomerLogin} />
          <Route exact path="/host/cart/:cartId" component={CustomerLogin} />
          <Layout>
            <Route exact path="/shop/info/view" component={ShopInfo} />
            <Route exact path="/shop/index/:shopId" component={ShopMain} />
            <Route exact path="/shop/item/add" component={AddEditItem} />
            <Route
              exact
              path="/shop/item/edit/:itemId"
              component={AddEditItem}
            />
            <Route exact path="/shop/orders/:shopId" component={OrdersList} />
            <Route exact path="/shop/order/:orderId" component={OrderDetail} />
            <Route
              exact
              path="/customer/host/noCart"
              component={CustomerHost}
            />
            <Route exact path="/customer/:cartId" component={CustomerMain} />
            <Route
              exact
              path="/customer/order/status/:orderId"
              component={OrderStatus}
            />
            <Route
              exact
              path="/customer/orders/view/list"
              component={CustomerOrderList}
            />
          </Layout>
        </Switch>
      </Suspense>
    </BrowserRouter>
  );
}

export default App;
