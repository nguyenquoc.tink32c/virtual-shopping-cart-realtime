import React from "react";

const Footer = () => {
  return (
    <footer
      className="page-footer text-center font-small= wow fadeIn bg bg-gray"
      style={{ backgroundColor: "#929FBA", visibility: "visible" }}
    >
      <div className="pb-4 pt-4">
        <a href="https://www.facebook.com/mdbootstrap">
          <i className="fab fa-facebook-f mr-3"></i>
        </a>

        <a href="https://twitter.com/MDBootstrap">
          <i className="fab fa-twitter mr-3"></i>
        </a>

        <a href="https://www.youtube.com/watch?v=7MUISDJ5ZZ4">
          <i className="fab fa-youtube mr-3"></i>
        </a>

        <a href="https://plus.google.com/u/0/b/107863090883699620484">
          <i className="fab fa-google-plus-g mr-3"></i>
        </a>

        <a href="https://dribbble.com/mdbootstrap">
          <i className="fab fa-dribbble mr-3"></i>
        </a>

        <a href="https://pinterest.com/mdbootstrap">
          <i className="fab fa-pinterest mr-3"></i>
        </a>

        <a href="https://github.com/mdbootstrap/bootstrap-material-design">
          <i className="fab fa-github mr-3"></i>
        </a>

        <a href="http://codepen.io/mdbootstrap/">
          <i className="fab fa-codepen mr-3"></i>
        </a>
      </div>
      <div
        className="footer-copyright py-3"
        style={{ backgroundColor: "#757F95" }}
      >
        © 2021 Copyright:
        <a href="https://mdbootstrap.com/education/bootstrap/">
          {" "}
          quocn.fsoft.com.vn
        </a>
      </div>
    </footer>
  );
};

export default Footer;
