import Footer from "./Footer";
import MainNavigation from "./MainNavigation";

const Layout = (props) => {
  return (
    <>
      <MainNavigation />
      <div className="lighten-3">
        <main className="mt-5 pt-4">
          <div className="container">{props.children}</div>
        </main>
      </div>
      <Footer />
    </>
  );
};

export default Layout;
