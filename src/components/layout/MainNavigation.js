import { isEqual } from "lodash";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { Link } from "react-router-dom";
import {
  LOGIN_TYPE_CUSTOMER,
  LOGIN_TYPE_SHOP,
  STORAGE_KEY_CART_ID,
  STORAGE_KEY_LOGIN_CUSTOMER_ID,
  STORAGE_KEY_LOGIN_SHOP_ID,
  STORAGE_KEY_LOGIN_TYPE,
} from "../../common/constants";
import localStorage from "../../common/localStorage";
import { createCart } from "../../redux/reducer/cartItemsReducer";
import { findShopInfoById } from "../../redux/reducer/shopReducer";
import QRCodeGenerate from "../qrcode/QRCode";
import Button from "../UI/Button";
import { CustomModal } from "../UI/CustomModal";

const MainNavigation = () => {
  const dispatch = useDispatch();
  const [openModal, setOpenModal] = useState(false);

  const [shareLink, setShareLink] = useState("");

  const cartId = localStorage.getStorage(STORAGE_KEY_CART_ID);
  const customerId = localStorage.getStorage(STORAGE_KEY_LOGIN_CUSTOMER_ID);
  const shopId = localStorage.getStorage(STORAGE_KEY_LOGIN_SHOP_ID);
  const loginType = localStorage.getStorage(STORAGE_KEY_LOGIN_TYPE);

  const loginInfo = useSelector((state) => state.shop.shopInfo);

  const history = useHistory();

  const handleShareClick = () => {
    if (isEqual(loginType, LOGIN_TYPE_SHOP)) {
      setShareLink("http://localhost:3000/host/shop/" + shopId);
      setOpenModal(!openModal);
    } else {
      if (cartId) {
        setShareLink("http://localhost:3000/host/cart/" + cartId);
        setOpenModal(!openModal);
      } else {
        alert("Please create cart before share it !");
      }
    }
  };

  const handleCreateCart = () => {
    const data = {
      customerId: customerId,
      shopId: shopId,
    };
    dispatch(createCart(data))
      .then((response) => {
        if (response.payload.errorMessage) {
          alert(response.payload.errorMessage);
        } else {
          localStorage.setStorage(STORAGE_KEY_CART_ID, response.payload.cartId);
        }
        history.push("/customer/" + response.payload.cartId);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  useEffect(() => {
    dispatch(findShopInfoById({ id: shopId }));
  }, [dispatch, shopId]);

  return (
    <nav className="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar top-nav-collapse">
      <div className="container">
        {loginInfo &&
          (isEqual(loginType, LOGIN_TYPE_SHOP) ? (
            <Link to={"/shop/index/" + shopId}>
              <img
                src={`data:image/jpeg;base64,${loginInfo.image}`}
                className="img-fluid"
                style={{ height: 28, width: 25 }}
                alt=""
              />
            </Link>
          ) : (
            <img
              src={`data:image/jpeg;base64,${loginInfo.image}`}
              className="img-fluid"
              style={{ height: 28, width: 25 }}
              alt=""
            />
          ))}
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
              {isEqual(loginType, LOGIN_TYPE_CUSTOMER) ? (
                <Link
                  to="/customer/orders/view/list"
                  className="nav-link waves-effect"
                >
                  {loginInfo && loginInfo.name}
                </Link>
              ) : (
                <Link to="/shop/info/view" className="nav-link waves-effect">
                  {loginInfo && loginInfo.name}
                </Link>
              )}
            </li>
            <li className="nav-item">
              {isEqual(loginType, LOGIN_TYPE_CUSTOMER) ? (
                <Link
                  to="/customer/orders/view/list"
                  className="nav-link waves-effect"
                >
                  List Orders
                </Link>
              ) : (
                <Link
                  to={"/shop/orders/" + shopId}
                  className="nav-link waves-effect"
                >
                  List Orders
                </Link>
              )}
            </li>
          </ul>
          <ul className="navbar-nav nav-flex-icons">
            <li className="nav-item">
              {isEqual(loginType, LOGIN_TYPE_CUSTOMER) && (
                <Button onClick={handleCreateCart}>
                  <i className="fas fa-shopping-cart"></i>{" "}
                  <span className="clearfix d-none d-sm-inline-block">
                    Create Cart
                  </span>
                </Button>
              )}
            </li>
            <li className="nav-item">
              <Button onClick={handleShareClick}>
                <i className="fas fa-share"></i>{" "}
                <span className="clearfix d-none d-sm-inline-block">Share</span>
              </Button>
            </li>
          </ul>
        </div>
      </div>
      {openModal && (
        <CustomModal
          setOpen={setOpenModal}
          title={
            isEqual(loginType, LOGIN_TYPE_CUSTOMER) ? "Cart Info" : "Shop Info"
          }
        >
          <p>
            {isEqual(loginType, LOGIN_TYPE_CUSTOMER) ? (
              <>
                <b>Cart ID : </b>
                <br />#{cartId}
              </>
            ) : (
              <>
                <b>Shop ID : </b>
                <br />#{shopId}
              </>
            )}
          </p>
          <p>
            <b>URL : </b>
            <br />
            {shareLink}
          </p>
          <div>
            <b>QRCode : </b>
            <br />
            <QRCodeGenerate text={shareLink} />
          </div>
        </CustomModal>
      )}
    </nav>
  );
};

export default MainNavigation;
