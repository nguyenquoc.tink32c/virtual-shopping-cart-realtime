import React from "react";
import QRCode from "qrcode.react";

const QRCodeGenerate = (props) => {
  return <QRCode value={props.text} />;
};

export default QRCodeGenerate;
