import React from "react";
import helpers from "../../../common/helpers";

const OrderStatusItems = (props) => {
  return (
    <>
      <table className="table">
        <thead>
          <tr>
            <th>Picture</th>
            <th>Name</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {props.items &&
            props.items.map((item, index) => (
              <tr key={index}>
                <td>
                  <img
                    src={`data:image/jpeg;base64,${item.image}`}
                    style={{ height: 60 }}
                    alt={"Image for " + item.itemName}
                  />
                </td>
                <td>
                  {item.itemName}
                  <br />
                  {helpers.formatCurrency(item.price)}
                </td>
                <td>
                  {item.amount}
                  <br />
                  {helpers.formatCurrency(item.amount * item.price)}
                </td>
              </tr>
            ))}
        </tbody>
      </table>
    </>
  );
};

export default OrderStatusItems;
