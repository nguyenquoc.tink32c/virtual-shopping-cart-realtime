import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import helpers from "../../../common/helpers";
import { findOrderById } from "../../../redux/reducer/orderDetailReducer";
import LoadingSpinner from "../../UI/LoadingSpinner";
import OrderStatusItems from "./OrderStatusItems";
import OrderStatusProgress from "./OrderStatusProgress";

const OrderStatus = () => {
  const { orderId } = useParams();
  const dispatch = useDispatch();

  const orderDetailInfo = useSelector((state) => state.orderDetail);

  useEffect(() => {
    dispatch(findOrderById({ id: orderId }));
  }, [dispatch, orderId]);

  return (
    <>
      {!orderDetailInfo.data && <LoadingSpinner />}
      {orderDetailInfo.data && (
        <div>
          <div>
            <h2 className="h1 fw-bold">Order Status</h2>
          </div>
          <div className="row shop-tracking-status">
            <OrderStatusProgress status={orderDetailInfo.data.status} />
          </div>
          <div className="row">
            <div>
              <OrderStatusItems items={orderDetailInfo.data.itemsInCart} />
            </div>
            <div>
              <div className="pt-4 card">
                <ul className="list-group list-group-flush">
                  <li className="list-group-item d-flex justify-content-between align-items-center border-0 px-0 mb-3 mr-5">
                    <div>
                      <strong className="pl-2">The total amount :</strong>
                    </div>
                    <br />
                    <span>
                      <strong>
                        {helpers.formatCurrency(
                          orderDetailInfo.data.totalPrice
                        )}
                      </strong>
                    </span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <br />
        </div>
      )}
    </>
  );
};

export default OrderStatus;
