import React from "react";
import {
  STATUS_CONFIRMED,
  STATUS_SEND_TO_KITCHEN,
  STATUS_READY_TO_DELIVERY,
  STATUS_READY_TO_PICKUP,
  STATUS_COMPLETED,
  STATUS_CANCELLED,
} from "../../../common/constants";

const OrderStatusProgress = (props) => {
  const getProgress = () => {
    switch (props.status) {
      case STATUS_CONFIRMED:
      case STATUS_CANCELLED:
        return "c1";
      case STATUS_SEND_TO_KITCHEN:
        return "c2";
      case STATUS_READY_TO_DELIVERY:
      case STATUS_READY_TO_PICKUP:
        return "c3";
      case STATUS_COMPLETED:
        return "c4";
      default:
        return "";
    }
  };

  return (
    <div className="order-status">
      <div className="order-status-timeline">
        <div
          className={"order-status-timeline-completion " + getProgress()}
        ></div>
      </div>

      <div className="image-order-status image-order-status-new active img-circle">
        <span className="status">Confirmed/Cancelled</span>
        <div className="icon"></div>
      </div>
      <div className="image-order-status image-order-status-active active img-circle">
        <span className="status">Send To Kitchen</span>
        <div className="icon"></div>
      </div>
      <div className="image-order-status image-order-status-intransit active img-circle">
        <span className="status">Ready for Pickup/Ready for Delivery</span>
        <div className="icon"></div>
      </div>
      <div className="image-order-status image-order-status-delivered active img-circle">
        <span className="status">Delivered</span>
        <div className="icon"></div>
      </div>
      <div className="image-order-status image-order-status-completed active img-circle">
        <span className="status">Completed</span>
        <div className="icon"></div>
      </div>
    </div>
  );
};

export default OrderStatusProgress;
