import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router";
import {
  STORAGE_KEY_LOGIN_CUSTOMER_ID,
  STORAGE_KEY_LOGIN_SHOP_ID,
} from "../../../common/constants";
import localStorage from "../../../common/localStorage";
import { findCartById } from "../../../redux/reducer/customerCartReducer";
import LoadingSpinner from "../../UI/LoadingSpinner";
import CartItems from "./CartItems";
import ItemList from "./ItemList";

const Index = () => {
  const { cartId } = useParams();
  const customerId = localStorage.getStorage(STORAGE_KEY_LOGIN_CUSTOMER_ID);
  const shopId = localStorage.getStorage(STORAGE_KEY_LOGIN_SHOP_ID);

  const dispatch = useDispatch();

  const customerCartInfo = useSelector((state) => state.customerCart);

  useEffect(() => {
    refreshItems();
  }, []);

  const refreshItems = () => {
    dispatch(findCartById({ id: cartId }))
      .then((response) => {
        if (response.payload.errorMessage) {
          alert(response.payload.errorMessage);
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  return (
    <>
      {customerCartInfo.isLoading && <LoadingSpinner />}
      {!customerCartInfo.isLoading && (
        <div className="row">
          <div className="col-md-7">
            <h2>Products</h2>
            <ItemList
              shopId={shopId}
              cartId={cartId}
              customerId={customerId}
              onRefreshItems={refreshItems}
            />
          </div>
          <div className="col-md-5">
            <section>
              <div className="container text-center">
                <h2 className="title-page">Shopping cart</h2>
              </div>
            </section>
            <CartItems
              data={customerCartInfo.data}
              cartId={cartId}
              onRefreshItems={refreshItems}
            />
          </div>
        </div>
      )}
    </>
  );
};

export default Index;
