import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import helpers from "../../../common/helpers";
import { findShopInfoById } from "../../../redux/reducer/shopReducer";
import { addItem } from "../../../redux/reducer/cartItemsReducer";

const ItemList = (props) => {
  const dispatch = useDispatch();
  const { shopInfo, isLoading } = useSelector((state) => state.shop);

  useEffect(() => {
    dispatch(findShopInfoById({ id: props.shopId }));
  }, [dispatch, props.shopId]);

  const handleAddItem = (itemId) => {
    const dataAdd = {
      itemId: itemId,
      cartId: props.cartId,
      customerId: props.customerId,
    };

    dispatch(addItem(dataAdd))
      .then((res) => {
        props.onRefreshItems();
      })
      .catch((e) => {
        console.log(e);
      });
  };

  return (
    <section className="section-content padding-y bg">
      <div className="row">
        {!isLoading &&
          shopInfo.items.map((item, index) => (
            <div key={item.itemId + index}>
              <div className="col-md-6">
                <div className="row mb-4">
                  <div className="col-md-4 col-lg-3 col-xl-5">
                    <div className="view zoom overlay z-depth-1 rounded mb-3 mb-md-0">
                      <img
                        src={`data:image/jpeg;base64,${item.image}`}
                        className="img-fluid w-100"
                        alt=""
                      />
                    </div>
                  </div>
                  <div className="col-md-8 col-lg-9 col-xl-7">
                    <div>
                      <div className="d-flex justify-content-between">
                        <div>
                          <h5>{item.name}</h5>
                          <p className="mb-0">
                            <span>
                              <strong id="summary">
                                {helpers.formatCurrency(item.price)}
                              </strong>
                            </span>
                          </p>
                        </div>
                      </div>
                      <div className="d-flex justify-content-between">
                        <div>
                          <button
                            className="btn btn-sm btn-outline-primary"
                            onClick={(e) => handleAddItem(item.itemId)}
                          >
                            Add to cart <i className="fa fa-shopping-cart"> </i>{" "}
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ))}
      </div>
    </section>
  );
};

export default ItemList;
