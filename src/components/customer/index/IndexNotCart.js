import React from "react";

const IndexNotCart = () => {
  return <div className="min-vh-100">You have not any cart !</div>;
};

export default IndexNotCart;
