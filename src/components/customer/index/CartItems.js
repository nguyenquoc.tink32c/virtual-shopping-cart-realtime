import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import lodash, { cloneDeep } from "lodash";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router";
import {
  STORAGE_KEY_LOGIN_CUSTOMER_ID,
  STORAGE_KEY_LOGIN_CUSTOMER_NAME,
} from "../../../common/constants";
import helpers from "../../../common/helpers";
import localStorage from "../../../common/localStorage";
import {
  removeItem,
  submitItem,
} from "../../../redux/reducer/cartItemsReducer";
import { placedOrder } from "../../../redux/reducer/orderReducer";
import Button from "../../UI/Button";

const CartItems = (props) => {
  const [groupData, setGroupData] = useState();
  const dispatch = useDispatch();
  const history = useHistory();
  const customerLoginId = localStorage.getStorage(
    STORAGE_KEY_LOGIN_CUSTOMER_ID
  );
  const customerLoginName = localStorage.getStorage(
    STORAGE_KEY_LOGIN_CUSTOMER_NAME
  );
  const [open, setOpen] = useState(false);

  const handleConfirmYes = () => {
    Object.entries(groupData).forEach(([customerName, items]) => {
      if (customerName === customerLoginName) {
        let itemsSubmit = [];
        let customerId;
        let cartId;
        items.forEach((item) => {
          itemsSubmit.push({
            amount: Number(item.amount),
            itemId: item.itemId,
            isDeleted: false,
          });
          customerId = item.customerId;
          cartId = item.cartId;
        });
        const dataSubmit = {
          items: itemsSubmit,
          customerId: customerId,
          cartId: cartId,
        };
        console.log(dataSubmit);
        dispatch(submitItem(dataSubmit))
          .then(() => {
            props.onRefreshItems();
          })
          .catch((e) => {
            console.log(e);
          });
      }
    });
    setOpen(false);
  };

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    setGroupData(lodash.groupBy(props.data.itemsInCart, "customerName"));
  }, [props.data]);

  const handleInputAmount = ({ amount }) => {
    Object.entries(groupData).forEach(([customerName]) => {
      if (customerName === customerLoginName) {
        const newState = cloneDeep(groupData);
        newState[customerName][0]["amount"] = amount;
        setGroupData(newState);
      }
    });
  };

  const handleDeleteItem = ({ customerId, itemId, cartId }) => {
    const dataRemove = {
      itemId: itemId,
      customerId: customerId,
      cartId: cartId,
    };

    dispatch(removeItem(dataRemove))
      .then(() => {
        props.onRefreshItems();
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleSubmit = () => {
    setOpen(true);
  };

  const handleOrder = () => {
    const data = {
      cartId: props.cartId,
      DeliveryInformation: "Please confirm this order !",
    };
    dispatch(placedOrder(data))
      .then((response) => {
        if (response.payload.errorMessage) {
          alert(response.payload.errorMessage);
        } else {
          alert("Order successfully!");
          history.push("/customer/order/status/" + props.cartId);
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const renderItems = (data) => {
    let journalEntries = [];

    Object.entries(data).forEach(([customerName, items]) => {
      if (customerName === customerLoginName) {
        pushItemInList(journalEntries, customerName, items, true);
      }
    });

    Object.entries(data).forEach(([customerName, items]) => {
      if (customerName !== customerLoginName) {
        pushItemInList(journalEntries, customerName, items, false);
      }
    });

    return <>{journalEntries}</>;
  };

  const calcTotalPrice = () => {
    let total = 0;
    Object.entries(groupData).forEach(([customerName, items]) => {
      items.forEach((item) => {
        total += item.price * item.amount;
      });
    });
    return total;
  };

  const pushItemInList = (journalEntries, customerName, items, isHost) => {
    journalEntries.push(
      <div key={customerName} className="row">
        <div>
          <div className="pl-2 pt-1">
            <i className="fas fa-user-alt"></i> {" " + customerName}{" "}
          </div>

          <table className="table table-borderless table-shopping-cart">
            <thead className="text-muted">
              <tr className="small text-uppercase">
                <th scope="col">Product</th>
                <th scope="col" width="120">
                  Quantity
                </th>
                <th scope="col" width="120">
                  Price
                </th>
                <th scope="col" className="text-right" width="200">
                  {" "}
                </th>
              </tr>
            </thead>
            <tbody>
              {items.map((item, index) => {
                return (
                  <tr key={customerName + item.itemId + index}>
                    <td>
                      <figure className="itemside align-items-center">
                        <figcaption className="info">
                          {item.itemName}
                        </figcaption>
                      </figure>
                    </td>
                    <td>
                      {isHost ? (
                        <input
                          type="text"
                          value={item.amount}
                          id={customerName + item.itemId + index}
                          onChange={(e) =>
                            handleInputAmount({
                              customerId: item.customerId,
                              itemId: item.itemId,
                              cartId: item.cartId,
                              amount: e.target.value,
                            })
                          }
                          className="form-control"
                          size="3"
                          maxLength="3"
                        />
                      ) : (
                        <label>{item.amount}</label>
                      )}
                    </td>
                    <td>
                      <div className="price-wrap">
                        <var className="price">
                          {helpers.formatCurrency(item.price * item.amount)}
                        </var>
                        <br />
                        <small className="text-muted">
                          {helpers.formatCurrency(item.price)} each{" "}
                        </small>
                      </div>
                    </td>
                    <td className="text-right">
                      {customerName === customerLoginName && (
                        <button
                          className="btn btn-light"
                          onClick={(e) =>
                            handleDeleteItem({
                              customerId: item.customerId,
                              itemId: item.itemId,
                              cartId: item.cartId,
                            })
                          }
                        >
                          Remove
                        </button>
                      )}
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    );
  };

  return (
    <div className="card">
      <div>
        <Dialog open={open} onClose={handleClose}>
          <DialogTitle>Dialog confirm</DialogTitle>
          <DialogContent>
            <DialogContentText>Do yout want to submit ?</DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              No
            </Button>
            <Button onClick={handleConfirmYes} color="primary" autoFocus>
              Yes
            </Button>
          </DialogActions>
        </Dialog>
      </div>

      {groupData && renderItems(groupData)}

      <div className="row">
        <div className="mb-3 ml-2">
          <div className="pt-4 mr-3">
            <ul className="list-group list-group-flush">
              <li className="list-group-item d-flex justify-content-between align-items-center border-0 px-0 mb-3 mr-5">
                <div>
                  <strong>The total amount :</strong>
                </div>
                <span>
                  <strong>
                    {groupData && helpers.formatCurrency(calcTotalPrice())}
                  </strong>
                </span>
              </li>
            </ul>
            {props.data.customerId !== customerLoginId && (
              <button
                type="button"
                className="btn btn-primary btn-block"
                onClick={handleSubmit}
              >
                Submit
              </button>
            )}
            {props.data.customerId === customerLoginId && (
              <button
                type="button"
                className="btn btn-primary btn-block"
                onClick={handleOrder}
              >
                Order
              </button>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default CartItems;
