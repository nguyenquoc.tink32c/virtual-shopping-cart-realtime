import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { STORAGE_KEY_LOGIN_CUSTOMER_ID } from "../../../common/constants";
import helpers from "../../../common/helpers";
import localStorage from "../../../common/localStorage";
import { getAllOrdersByCustomerId } from "../../../redux/reducer/orderReducer";
import LoadingSpinner from "../../UI/LoadingSpinner";

const OrderList = () => {
  const dispatch = useDispatch();
  const customerId = localStorage.getStorage(STORAGE_KEY_LOGIN_CUSTOMER_ID);

  const orderInfo = useSelector((state) => state.order);

  useEffect(() => {
    dispatch(getAllOrdersByCustomerId({ id: customerId }));
  }, [dispatch, customerId]);

  return (
    <>
      {orderInfo.isLoading && <LoadingSpinner />}
      {!orderInfo.isLoading && (
        <div className="min-vh-100">
          <div>
            <h2 className="h1 fw-bold">List Orders</h2>
          </div>
          <table className="table">
            <thead>
              <tr>
                <th>#</th>
                <th>Customer Name</th>
                <th>Customer Phone</th>
                <th>Total</th>
                <th>Status</th>
                <th>Order Time</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {orderInfo.data.orders &&
                orderInfo.data.orders.map((order) => (
                  <tr key={order.orderId}>
                    <td>{helpers.formatId(order.orderId)}</td>
                    <td>{order.customerName}</td>
                    <td>{order.customerPhoneNumber}</td>
                    <td>{helpers.formatCurrency(order.totalPrice)}</td>
                    <td>{helpers.getStatusName(order.status)}</td>
                    <td>{helpers.formatDateTime(order.orderTime)}</td>
                    <td>
                      <Link to={"/customer/order/status/" + order.orderId}>
                        View
                      </Link>
                    </td>
                  </tr>
                ))}
            </tbody>
          </table>
        </div>
      )}
    </>
  );
};

export default OrderList;
