import classes from "./LoadingSpinner.module.css";

const LoadingSpinner = () => {
  return (
    <div className="container text-center">
      <div className={classes.spinner}></div>
    </div>
  );
};

export default LoadingSpinner;
