import React from "react";

import classes from "./Button.module.css";

const Button = (props) => {
  return (
    <button
      className={
        classes.button +
        " btn btn-md my-0 p waves-effect waves-light " +
        props.classEtx
      }
      type={props.type || "button"}
      onClick={props.onClick}
    >
      {props.children}
    </button>
  );
};

export default Button;
