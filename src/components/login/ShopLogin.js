import React from "react";
import Login from "./Login";

import { LOGIN_TYPE_SHOP } from "../../common/constants";

const ShopLogin = () => {
  return <Login loginType={LOGIN_TYPE_SHOP} />;
};

export default ShopLogin;
