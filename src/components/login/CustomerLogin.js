import React from "react";
import { useParams } from "react-router";

import { LOGIN_TYPE_CUSTOMER } from "../../common/constants";
import Login from "./Login";

const CustomerLogin = () => {
  const params = useParams();
  return <Login loginType={LOGIN_TYPE_CUSTOMER} params={params} />;
};

export default CustomerLogin;
