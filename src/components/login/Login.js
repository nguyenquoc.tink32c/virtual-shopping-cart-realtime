import React, { useRef } from "react";
import { useDispatch } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import {
  LOGIN_TYPE_CUSTOMER,
  LOGIN_TYPE_SHOP,
  STORAGE_KEY_CART_ID,
  STORAGE_KEY_LOGIN_CUSTOMER_ID,
  STORAGE_KEY_LOGIN_CUSTOMER_NAME,
  STORAGE_KEY_LOGIN_SHOP_ID,
  STORAGE_KEY_LOGIN_TYPE,
} from "../../common/constants";
import localStorage from "../../common/localStorage";
import { findCartById } from "../../redux/reducer/customerCartReducer";
import { loginCustomer, loginShop } from "../../redux/reducer/loginReducer";

const Login = (props) => {
  const history = useHistory();
  const phoneNumber = useRef("");

  const dispatch = useDispatch();

  const handleLogin = (e) => {
    e.preventDefault();

    if (!isValidate()) {
    }

    if (props.loginType === LOGIN_TYPE_SHOP) {
      dispatch(
        loginShop({
          phoneNumber: phoneNumber.current.value,
        })
      )
        .then((response) => {
          history.push("/shop/index/" + response.payload.shopId);
          localStorage.setStorage(
            STORAGE_KEY_LOGIN_SHOP_ID,
            response.payload.shopId
          );
          localStorage.setStorage(STORAGE_KEY_LOGIN_TYPE, LOGIN_TYPE_SHOP);
          localStorage.removeStorage(STORAGE_KEY_CART_ID);
        })
        .catch((e) => {
          console.log(e);
        });
    } else {
      dispatch(
        loginCustomer({
          phoneNumber: phoneNumber.current.value,
        })
      )
        .then((response) => {
          localStorage.setStorage(
            STORAGE_KEY_LOGIN_CUSTOMER_ID,
            response.payload.customerId
          );
          localStorage.setStorage(
            STORAGE_KEY_LOGIN_CUSTOMER_NAME,
            response.payload.name
          );
          localStorage.setStorage(STORAGE_KEY_LOGIN_TYPE, LOGIN_TYPE_CUSTOMER);

          if (props.params.shopId !== undefined) {
            localStorage.setStorage(
              STORAGE_KEY_LOGIN_SHOP_ID,
              props.params.shopId
            );
            history.push("/customer/orders/view/list");
          } else if (props.params.cartId !== undefined) {
            localStorage.setStorage(STORAGE_KEY_CART_ID, props.params.cartId);

            dispatch(findCartById({ id: props.params.cartId }))
              .then((response) => {
                localStorage.setStorage(
                  STORAGE_KEY_LOGIN_SHOP_ID,
                  response.payload.shopId
                );
              })
              .catch((e) => {
                console.log(e);
              });
            history.push("/customer/" + props.params.cartId);
          }
        })
        .catch((e) => {
          console.log(e);
        });
    }
  };

  const isValidate = () => {
    const enteredPhoneNumber = phoneNumber.current.value;

    if (enteredPhoneNumber.length < 10) {
      alert("Please input a valid phone number ! (Length >= 10) ");
    }
  };

  return (
    <>
      <section
        className="section-conten padding-y"
        style={{ minHeight: 84, paddingTop: 150 }}
      >
        <div className="card mx-auto w-25 mt-10">
          <div className="card-body">
            <h4 className="card-title mb-4 text-center">Sign in</h4>
            <form onSubmit={handleLogin}>
              <a
                className="btn btn-facebook btn-block mb-2"
                style={{ backgroundColor: "#405D9D", color: "white" }}
              >
                {" "}
                <i className="fab fa-facebook"></i> &nbsp; Sign in with Facebook
              </a>
              <a
                className="btn btn-google btn-block mb-4"
                style={{ backgroundColor: "#ff7575", color: "white" }}
              >
                {" "}
                <i className="fab fa-google"></i> &nbsp; Sign in with Google
              </a>
              <div className="form-group">
                <input
                  name=""
                  className="form-control"
                  placeholder="Phone Number"
                  type="text"
                  id="phoneNumber"
                  ref={phoneNumber}
                />
              </div>

              <div className="form-group">
                <button type="submit" className="btn btn-primary btn-block">
                  {" "}
                  Login{" "}
                </button>
              </div>
            </form>
          </div>
        </div>

        {props.loginType === LOGIN_TYPE_SHOP && (
          <p className="text-center mt-4">
            Don't have account? <Link to="/shop/regist">Sign up</Link>
          </p>
        )}
        <br />
        <br />
      </section>
    </>
  );
};

export default Login;
