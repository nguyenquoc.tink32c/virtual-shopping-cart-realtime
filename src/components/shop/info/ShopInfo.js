import React, { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { STORAGE_KEY_LOGIN_SHOP_ID } from "../../../common/constants";
import localStorage from "../../../common/localStorage";
import {
  findShopInfoById,
  updateShop,
} from "../../../redux/reducer/shopReducer";
import LoadingSpinner from "../../UI/LoadingSpinner";

const ShopInfo = () => {
  const dispatch = useDispatch();
  const shopId = localStorage.getStorage(STORAGE_KEY_LOGIN_SHOP_ID);
  const { isLoading } = useSelector((state) => state.shop);
  const inputNameRef = useRef("");
  const inputPhoneNumberRef = useRef("");
  const inputNewPhoneNumberRef = useRef("");
  const [imageDisplay, setImageDisplay] = useState("");
  const [imageSelected, setImageSelected] = useState(null);
  const [isEditMode, setIsEditMode] = useState(false);

  useEffect(() => {
    dispatch(findShopInfoById({ id: shopId }))
      .then((response) => {
        inputNameRef.current.value = response.payload.name;
        inputPhoneNumberRef.current.value = response.payload.phoneNumber;
        setImageDisplay(`data:image/jpeg;base64,${response.payload.image}`);
      })
      .catch((e) => {
        console.log(e);
      });
  }, []);

  const handleFileInput = (files) => {
    setImageSelected(files[0]);
    setImageDisplay(URL.createObjectURL(files[0]));
  };

  const handleSaveShopInfo = () => {
    let formData = new FormData();
    formData.append("Name", inputNameRef.current.value);
    formData.append("PhoneNumber", inputPhoneNumberRef.current.value);

    if (imageSelected) {
      formData.append("Logo", imageSelected, imageSelected.name);
    }

    if (inputNewPhoneNumberRef.current.value) {
      formData.append("NewPhoneNumber", inputNewPhoneNumberRef.current.value);
    }

    dispatch(updateShop(formData));
  };

  const handleEditMode = () => {
    setIsEditMode(!isEditMode);
  };

  return (
    <>
      {isLoading && <LoadingSpinner />}
      {!isLoading && (
        <div className="container dark-grey-text mt-5">
          <div className="row">
            <div className="col-md-6">
              <h1>Shop Profile</h1>
            </div>
            <div className="col-md-6 text-right">
              <button
                type="button"
                onClick={handleEditMode}
                className="btn btn-light mr-4"
              >
                <i className="fas fa-edit"></i>
              </button>
            </div>
          </div>
          <div className="row wow fadeIn">
            <div className="col-md-6 mb-4 d-flex justify-content-center">
              <img
                id="image"
                src={imageDisplay}
                style={{ height: 250 }}
                alt="Image"
              />
            </div>
            <div className="col-md-6 mb-4">
              <div className="p-4">
                <form
                  className="justify-content-left"
                  onSubmit={handleSaveShopInfo}
                >
                  <div className="form-group">
                    {isEditMode && (
                      <p>
                        Please fill in this form to update information shop!
                      </p>
                    )}
                  </div>
                  <div className="form-group">
                    <input
                      type="text"
                      id="name"
                      ref={inputNameRef}
                      disabled={!isEditMode}
                      className="form-control"
                      placeholder="Shop Name"
                    />
                  </div>
                  <div className="form-group">
                    <input
                      type="text"
                      id="phoneNumber"
                      ref={inputPhoneNumberRef}
                      disabled={true}
                      className="form-control"
                      placeholder="Phone Number"
                    />
                  </div>
                  {isEditMode && (
                    <div className="form-group">
                      <input
                        type="text"
                        id="newPhoneNumber"
                        ref={inputNewPhoneNumberRef}
                        className="form-control"
                        placeholder="Phone New Number"
                      />
                    </div>
                  )}
                  <div className="form-group">
                    <input
                      type="file"
                      onChange={(e) => handleFileInput(e.target.files)}
                      disabled={!isEditMode}
                      className="form-control"
                    />
                  </div>
                  <button
                    type="submit"
                    className="btn btn-primary btn-md my-0 p ml-0"
                    disabled={!isEditMode}
                  >
                    Save
                  </button>
                </form>
              </div>
            </div>
          </div>
          <div className="row d-flex justify-content-center wow fadeIn">
            <div className="col-md-6 text-center">
              <h4 className="my-4 h4">Additional information</h4>

              <p>
                The Choreograph collection makes it easier than ever to create a
                shower space that caters to your needs, your style, and your
                life. Choreograph shower walls and accessories let you
                personalize your shower with beautiful wall designs and
                thoughtfully designed storage.
              </p>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default ShopInfo;
