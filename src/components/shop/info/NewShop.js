import React, { useRef, useState } from "react";
import { useDispatch } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { registShop } from "../../../redux/reducer/shopReducer";

const NewShop = () => {
  const inputNameRef = useRef("");
  const inputPhoneNumberRef = useRef("");
  const [imageDisplay, setImageDisplay] = useState("");
  const [imageSelected, setImageSelected] = useState(null);

  const dispatch = useDispatch();
  const history = useHistory();

  const handleFileInput = (files) => {
    setImageSelected(files[0]);
    setImageDisplay(URL.createObjectURL(files[0]));
  };

  const handleRegisterShopInfo = () => {
    let formData = new FormData();
    formData.append("Name", inputNameRef.current.value);
    formData.append("PhoneNumber", inputPhoneNumberRef.current.value);

    if (imageSelected) {
      formData.append("Logo", imageSelected, imageSelected.name);
    }

    dispatch(registShop(formData));
    history.push("/shop/login");
  };

  return (
    <>
      <section
        className="section-conten padding-y"
        style={{ minHeight: 84, paddingTop: 20 }}
      >
        <div className="card mx-auto w-25 mt-10">
          <div className="card-body"></div>
          <div className="signup-form" style={{ margin: 30 }}>
            <form onSubmit={handleRegisterShopInfo}>
              <h2>Sign Up</h2>
              <p>Please fill in this form to create an account!</p>
              <hr />
              <div className="form-group">
                <div className="row">
                  <div className="col">
                    <input
                      type="text"
                      id="name"
                      ref={inputNameRef}
                      className="form-control"
                      placeholder="Shop Name"
                    />
                  </div>
                </div>
              </div>
              <div className="form-group">
                <input
                  type="text"
                  className="form-control"
                  name="phoneNumber"
                  id="phoneNumber"
                  placeholder="Phone Number"
                  required="required"
                  ref={inputPhoneNumberRef}
                />
              </div>
              <div className="form-group">
                <input
                  type="file"
                  onChange={(e) => handleFileInput(e.target.files)}
                  className="form-control"
                />
                <br />
                {imageSelected ? (
                  <img
                    id="image"
                    src={imageDisplay}
                    style={{ height: 300 }}
                    alt="Image"
                    className="form-control"
                  />
                ) : (
                  <img src="/default.jpeg" height="320px" width="320px" />
                )}
              </div>
              <div className="form-group">
                <label className="form-check-label">
                  <input type="checkbox" required="required" /> I accept the{" "}
                  <a>Terms of Use</a> &amp; <a>Privacy Policy</a>
                </label>
              </div>
              <div className="form-group">
                <button type="submit" className="btn btn-primary btn-lg">
                  Sign Up
                </button>
              </div>
            </form>
            <div className="hint-text">
              Already have an account? <Link to="/shop/login">Login here</Link>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default NewShop;
