import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import helpers from "../../../common/helpers";
import { deleteItem } from "../../../redux/reducer/itemReducer";
import { findShopInfoById } from "../../../redux/reducer/shopReducer";
import LoadingSpinner from "../../UI/LoadingSpinner";

const ItemList = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { shopInfo, isLoading } = useSelector((state) => state.shop);
  const { data } = useSelector((state) => state.item);

  useEffect(() => {
    dispatch(findShopInfoById({ id: props.shopId }));
  }, [dispatch, props.shopId, data]);

  const handleDeleteItem = (itemId) => {
    dispatch(
      deleteItem({
        shopId: props.shopId,
        itemId: itemId,
      })
    );
  };

  const handleUpdateItem = (itemId) => {
    history.push("/shop/item/edit/" + itemId);
  };

  return (
    <>
      {isLoading && !shopInfo && <LoadingSpinner />}
      {!isLoading && shopInfo && (
        <section className="text-center">
          <div className="row d-flex">
            {shopInfo &&
              shopInfo.items
                .filter((i) => i.isActive === true)
                .map((item, index) => (
                  <div className="col-md-3 mb-3 d-flex" key={index}>
                    <div className="">
                      <div className="view zoom overlay z-depth-2 rounded">
                        <img
                          src={`data:image/jpeg;base64,${item.image}`}
                          className="img-fluid w-100"
                          style={{ height: 350 }}
                          alt=""
                        />
                      </div>
                      <div className="text-center pt-4">
                        <h5 className="text_ellipsis"> {item.name}</h5>
                        <hr />
                        <h6 className="mb-3">
                          <strong>{helpers.formatCurrency(item.price)}</strong>
                        </h6>
                        <button
                          type="button"
                          className="btn btn-primary btn-sm mr-1 mb-2"
                          onClick={() => handleUpdateItem(item.itemId)}
                        >
                          <i className="fas fa-edit pr-2"></i>Edit
                        </button>
                        <button
                          type="button"
                          className="btn btn-danger btn-sm mr-1 mb-2"
                          onClick={() => handleDeleteItem(item.itemId)}
                        >
                          <i className="fas fa-trash-alt pr-2"></i>Delete
                        </button>
                      </div>
                    </div>
                  </div>
                ))}
          </div>
        </section>
      )}
    </>
  );
};

export default ItemList;
