import React, { useEffect, useRef, useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory, useParams } from "react-router";
import { STORAGE_KEY_LOGIN_SHOP_ID } from "../../../common/constants";
import localStorage from "../../../common/localStorage";
import {
  createItem,
  findItemById,
  updateItem,
} from "../../../redux/reducer/itemReducer";

const AddEditItem = () => {
  const inputNameRef = useRef("");
  const inputPriceRef = useRef("");
  const [imageSelected, setImageSelected] = useState(null);
  const [imageDisplay, setImageDisplay] = useState(null);

  const { itemId } = useParams();
  const isAddNew = !itemId;

  const dispatch = useDispatch();
  const history = useHistory();
  const shopId = localStorage.getStorage(STORAGE_KEY_LOGIN_SHOP_ID);

  const handleFileInput = (files) => {
    setImageSelected(files[0]);
    setImageDisplay(URL.createObjectURL(files[0]));
  };

  const handleSaveItem = () => {
    let formData = new FormData();
    formData.append("ShopId", shopId);
    formData.append("Name", inputNameRef.current.value);
    formData.append("Price", inputPriceRef.current.value);

    if (imageSelected) {
      formData.append("Image", imageSelected, imageSelected.name);
    }

    if (isAddNew) {
      createNewItem(formData);
    } else {
      formData.append("ItemId", itemId);
      editItem(formData);
    }

    history.push("/shop/index/" + shopId);
  };

  const createNewItem = (data) => {
    dispatch(createItem(data));
  };

  const editItem = (data) => {
    dispatch(updateItem(data));
  };

  useEffect(() => {
    if (!isAddNew) {
      dispatch(findItemById(itemId)).then((response) => {
        inputNameRef.current.value = response.payload.name;
        inputPriceRef.current.value = response.payload.price;

        setImageDisplay(`data:image/jpeg;base64,${response.payload.image}`);
      });
    }
  }, [dispatch]);

  return (
    <>
      {
        <div className="container dark-grey-text">
          <div className="row">
            <div className="col-md-6">
              <h2>{isAddNew ? "New Product" : "Edit Product"}</h2>
              <p>Please fill in this form to add a product!</p>
            </div>
          </div>
          <div className="row wow fadeIn">
            <div className="col-md-6 mb-4 d-flex justify-content-center">
              {imageDisplay ? (
                <img
                  id="image"
                  src={imageDisplay}
                  className="img-fluid"
                  alt="Image"
                  height="50%"
                  width="40%"
                />
              ) : (
                <img src="/default.jpeg" />
              )}
            </div>
            <div className="col-md-6 mb-4">
              <div className="p-4">
                <form
                  className="justify-content-left"
                  onSubmit={handleSaveItem}
                >
                  <div className="form-group">
                    <label htmlFor="name">Name</label>
                    <input
                      type="text"
                      id="name"
                      required
                      ref={inputNameRef}
                      className="form-control"
                      placeholder="Product Name"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="price">Price ($)</label>
                    <input
                      type="number"
                      id="price"
                      required
                      ref={inputPriceRef}
                      min="0"
                      step="0.01"
                      className="form-control"
                      placeholder="0.01"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="fileDownload">Image</label>
                    <input
                      type="file"
                      onChange={(e) => handleFileInput(e.target.files)}
                      className="form-control form-control-sm"
                    />
                  </div>
                  <button
                    type="submit"
                    className="btn btn-primary btn-md my-0 p ml-0"
                  >
                    Save
                  </button>
                </form>
              </div>
            </div>
          </div>

          <hr />

          <div className="row d-flex justify-content-center wow fadeIn">
            <div className="col-md-6 text-center">
              <h4 className="my-4 h4">Additional information</h4>

              <p>
                The Choreograph collection makes it easier than ever to create a
                shower space that caters to your needs, your style, and your
                life. Choreograph shower walls and accessories let you
                personalize your shower with beautiful wall designs and
                thoughtfully designed storage.
              </p>
            </div>
          </div>
        </div>
      }
    </>
  );
};

export default AddEditItem;
