import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import { Link } from "react-router-dom";

import helpers from "../../../common/helpers";
import { findOrderById } from "../../../redux/reducer/orderDetailReducer";
import {
  updateStatus,
  cancelStatus,
} from "../../../redux/reducer/orderReducer";
import {
  STATUS_CONFIRMED,
  STATUS_SEND_TO_KITCHEN,
  STATUS_READY_TO_DELIVERY,
  STATUS_READY_TO_PICKUP,
  STATUS_COMPLETED,
  STATUS_CANCELLED,
} from "../../../common/constants";
import LoadingSpinner from "../../UI/LoadingSpinner";

const OrderDetail = () => {
  const [statusSelected, setStatusSelected] = useState();
  const { orderId } = useParams();
  const dispatch = useDispatch();

  const orderDetailInfo = useSelector((state) => state.orderDetail);

  useEffect(() => {
    dispatch(findOrderById({ id: orderId }));
  }, [dispatch, orderId]);

  const handlerChangeStatus = (e) => {
    const status = e.target.value;
    const customerId = orderDetailInfo.data.customerId;
    const shopId = orderDetailInfo.data.shopId;

    if (status === STATUS_CANCELLED) {
      dispatch(
        cancelStatus({
          orderId: orderId,
          customerId: customerId,
        })
      )
        .then(() => {
          alert(`The status "Cancelled" is updated !`);
        })
        .catch((e) => {
          alert(e);
        });
    } else {
      dispatch(
        updateStatus({
          orderId: orderId,
          orderStatus: status,
          customerId: customerId,
          shopId: shopId,
        })
      )
        .then(() => {
          alert(`The status "${helpers.getStatusName(status)}" is updated !`);
        })
        .catch((e) => {
          alert(e);
        });
    }

    setStatusSelected(status);
  };

  return (
    <>
      {orderDetailInfo.isLoading && <LoadingSpinner />}
      {!orderDetailInfo.isLoading && (
        <div className="vh-100">
          <div className="mb-2">
            <Link to={"/shop/orders/" + orderDetailInfo.data.shopId}>
              Order List
            </Link>
          </div>
          <div className="card pt-2 pr-2 pl-2 pb-2">
            <p>
              <b>Order Number : </b>
              <span>{orderId}</span>
            </p>
            <p>
              <b>Customer Name : </b>
              <span>{orderDetailInfo.data.customerName}</span>
            </p>
            <p>
              <b>Customer Phone : </b>
              <span>{orderDetailInfo.data.customerPhoneNumber}</span>
            </p>
            <p>
              <b>Order Time : </b>
              <span>
                {helpers.formatDateTime(orderDetailInfo.data.orderTime)}
              </span>
            </p>
            <div>
              <b>Status : </b>
              <select
                value={
                  statusSelected ? statusSelected : orderDetailInfo.data.status
                }
                className="form-select w-25"
                onChange={handlerChangeStatus}
              >
                <option value=""></option>
                <option value={STATUS_CONFIRMED}>Confirmed</option>
                <option value={STATUS_SEND_TO_KITCHEN}>Send to kitchen</option>
                <option value={STATUS_READY_TO_DELIVERY}>
                  Ready to delivery
                </option>
                <option value={STATUS_READY_TO_PICKUP}>Ready to pickup</option>
                <option value={STATUS_COMPLETED}>Completed</option>
                <option value={STATUS_CANCELLED}>Cancelled</option>
              </select>
            </div>
          </div>
          <div className="mt-4">
            <div>
              <p className="h3 fw-bold">Products</p>
            </div>
            <table className="table">
              <thead>
                <tr>
                  <th>Picture</th>
                  <th>Item</th>
                  <th>Price</th>
                  <th>Quantity</th>
                  <th>Sub Total</th>
                </tr>
              </thead>
              <tbody>
                {orderDetailInfo.data.itemsInCart.map((item, index) => (
                  <tr key={index}>
                    <td>
                      <img
                        src={`data:image/jpeg;base64,${item.image}`}
                        style={{ height: 80, width: 80 }}
                        alt={"Image for " + item.name}
                      />
                    </td>
                    <td>{item.itemName}</td>
                    <td>{helpers.formatCurrency(item.price)}</td>
                    <td>{item.amount}</td>
                    <td>{helpers.formatCurrency(item.price * item.amount)}</td>
                  </tr>
                ))}
              </tbody>
            </table>
            <p>
              <b>Total: </b>
              <span>
                {helpers.formatCurrency(orderDetailInfo.data.totalPrice)}
              </span>
            </p>
          </div>
        </div>
      )}
    </>
  );
};

export default OrderDetail;
