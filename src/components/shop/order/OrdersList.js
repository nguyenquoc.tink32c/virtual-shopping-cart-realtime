import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";

import helpers from "../../../common/helpers";
import { findAllOrdersByShopId } from "../../../redux/reducer/orderReducer";
import { useParams } from "react-router";
import LoadingSpinner from "../../UI/LoadingSpinner";

const OrdersList = () => {
  const dispatch = useDispatch();
  const { shopId } = useParams();

  const orderInfo = useSelector((state) => state.order);

  useEffect(() => {
    dispatch(findAllOrdersByShopId({ id: shopId }));
  }, [dispatch, shopId]);

  return (
    <>
      {orderInfo.isLoading && <LoadingSpinner />}
      {!orderInfo.isLoading && (
        <div className="min-vh-100">
          <Link to={"/shop/index/" + shopId}>Menu</Link>
          <table className="table">
            <thead>
              <tr>
                <th>#</th>
                <th>Customer Name</th>
                <th>Customer Phone</th>
                <th>Total</th>
                <th>Status</th>
                <th>Order Time</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {orderInfo.data.orders &&
                orderInfo.data.orders.map((order) => (
                  <tr key={order.orderId}>
                    <td>{helpers.formatId(order.orderId)}</td>
                    <td>{order.customerName}</td>
                    <td>{order.customerPhoneNumber}</td>
                    <td>{helpers.formatCurrency(order.totalPrice)}</td>
                    <td>{helpers.getStatusName(order.status)}</td>
                    <td>{helpers.formatDateTime(order.orderTime)}</td>
                    <td>
                      <Link to={"/shop/order/" + order.orderId}>View</Link>
                    </td>
                  </tr>
                ))}
            </tbody>
          </table>
        </div>
      )}
    </>
  );
};

export default OrdersList;
