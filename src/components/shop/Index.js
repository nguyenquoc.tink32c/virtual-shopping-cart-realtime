import React from "react";

import { useHistory, useParams } from "react-router";
import { Link } from "react-router-dom";
import ItemsList from "./item/ItemList";
import Button from "../UI/Button";
const Index = () => {
  const { shopId } = useParams();
  const history = useHistory();

  const addNewItem = () => {
    history.push("/shop/item/add");
  };

  return (
    <>
      <div className="row">
        <div className="col-md-6">
          <h2 className="h1 fw-bold">Products</h2>
        </div>
        <div className="col-md-6 text-right">
          <Button onClick={addNewItem}>
            <i className="fas fa-plus-circle"></i> {" Add Products"}
          </Button>
        </div>
      </div>
      <br />
      <ItemsList shopId={shopId} />
    </>
  );
};

export default Index;
