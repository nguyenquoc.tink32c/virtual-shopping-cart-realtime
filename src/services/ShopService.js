import http from "../common/http";

// Get shop info by shop Id
const findById = (id) => {
  return http.get(`/Shop/${id}`);
};

// Login by phoneNumber
const login = (data) => {
  return http.post("/Shop/login", data);
};

// Login by phoneNumber
const create = (data) => {
  return http.post("/Shop/register", data);
};

// Login by phoneNumber
const update = (data) => {
  return http.put("/Shop", data);
};

// Define service
const ShopService = {
  findById,
  login,
  create,
  update,
};

export default ShopService;
