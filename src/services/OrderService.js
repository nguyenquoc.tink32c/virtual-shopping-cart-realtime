import http from "../common/http";

// Get the list items in cart
const getOrderById = (id) => {
  return http.get(`/Order/${id}`);
};

// Placed new order
const placedNewOrder = (data) => {
  return http.post("/Order", data);
};

// Update the order status
const updateOrderStatus = (data) => {
  return http.put("/Order/status", JSON.stringify(data));
};

// Cancel the order status
const cancelOrderStatus = (data) => {
  return http.put("/Order/cancel", JSON.stringify(data));
};

// Get the list items in cart
const getAllOrdersByCutomserId = (id) => {
  return http.get(`/Order/${id}/customer/all`);
};

// Get the list items in cart
const getAllOrdersByShopId = (id) => {
  return http.get(`/Order/${id}/shop/all`);
};

// Define service
const OrderService = {
  getOrderById,
  placedNewOrder,
  updateOrderStatus,
  cancelOrderStatus,
  getAllOrdersByCutomserId,
  getAllOrdersByShopId,
};

export default OrderService;
