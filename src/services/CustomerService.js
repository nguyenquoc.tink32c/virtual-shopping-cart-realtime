import http from "../common/http";

// Login by phoneNumber
const login = (data) => {
  return http.post("/Customer/login", JSON.stringify(data));
};

// Define service
const CustomerService = {
  login,
};

export default CustomerService;
