import http from "../common/http";

const get = (id) => {
  return http.get(`/Item/${id}`);
};

const create = (data) => {
  return http.post("/Item/create", data);
};

const update = (data) => {
  return http.put("/Item", data);
};

const remove = ({ shopId, itemId }) => {
  return http.delete("/Item", { data: { shopId: shopId, itemId: itemId } });
};

const active = (data) => {
  return http.put("/Item/active", data);
};

const ItemService = {
  get,
  create,
  update,
  remove,
  active,
};

export default ItemService;
