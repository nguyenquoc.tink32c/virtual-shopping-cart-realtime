import http from "../common/http";

// Get Cart by Id
const getCartById = (id) => {
  return http.get(`/Cart/${id}`);
};

// Create new cart
const create = (data) => {
  return http.post("/Cart/create", data);
};

// Add an item into cart
const addOneItem = (data) => {
  return http.post("/Cart/add/item", data);
};

// Submit item into cart (Submit items and quantity)
const submit = (data) => {
  return http.post("/Cart/submit", data);
};

// Remove item in cart
const removeItem = (data) => {
  return http.post("/Cart/remove/item", data);
};

// Get exits cart
const getExistCart = (data) => {
  return http.post("/Cart/exist/shop/customer", data);
};

// Remove co-making customer
const removeCoMaking = (data) => {
  return http.post("/Cart/remove/customer", data);
};

// Define service
const CartService = {
  getCartById,
  create,
  addOneItem,
  submit,
  removeItem,
  getExistCart,
  removeCoMaking,
};

export default CartService;
