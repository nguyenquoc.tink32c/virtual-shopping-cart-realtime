import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import CartService from "../../services/CartService";

const initialState = {
  data: null,
  isLoading: true,
};

export const findCartById = createAsyncThunk(
  "Carts/findCartById",
  async ({ id }) => {
    const res = await CartService.getCartById(id);
    return res.data;
  }
);

const customerCartSlice = createSlice({
  name: "customerCart",
  initialState: initialState,
  extraReducers: {
    [findCartById.fulfilled]: (state, action) => {
      return {
        data: action.payload,
        isLoading: false,
      };
    },
  },
});

const { reducer } = customerCartSlice;

export default reducer;
