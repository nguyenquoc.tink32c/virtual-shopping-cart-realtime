import ShopService from "../../services/ShopService";
import CustomerService from "../../services/CustomerService";
import { LOGIN_TYPE_SHOP, LOGIN_TYPE_CUSTOMER } from "../../common/constants";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

const initialState = {
  shop: null,
  customer: null,
  loginType: null,
};

export const loginShop = createAsyncThunk(
  "Login/shop",
  async ({ phoneNumber }) => {
    let res = await ShopService.login({ phoneNumber: phoneNumber });
    let shopId = "";
    if (res.data) {
      shopId = res.data.shopId;
      res = await ShopService.findById(shopId);
    }
    const data = res.data;
    return { ...data, shopId };
  }
);

export const loginCustomer = createAsyncThunk(
  "Login/customer",
  async ({ phoneNumber }) => {
    const res = await CustomerService.login({ phoneNumber: phoneNumber });

    return res.data;
  }
);

const loginSlice = createSlice({
  name: "login",
  initialState: initialState,
  extraReducers: {
    [loginShop.fulfilled]: (state, action) => {
      return {
        ...state,
        shop: action.payload,
        customer: null,
        isLoading: false,
        loginType: LOGIN_TYPE_SHOP,
      };
    },
    [loginCustomer.fulfilled]: (state, action) => {
      return {
        ...state,
        shop: null,
        customer: action.payload,
        isLoading: false,
        loginType: LOGIN_TYPE_CUSTOMER,
      };
    },
  },
});

const { reducer } = loginSlice;

export default reducer;
