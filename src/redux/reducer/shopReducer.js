import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import ShopService from "../../services/ShopService";

const initialState = {
  shopInfo: null,
  isLoading: true,
};

export const findShopInfoById = createAsyncThunk(
  "Shop/findById",
  async ({ id }) => {
    const res = await ShopService.findById(id);
    return res.data;
  }
);

export const registShop = createAsyncThunk("Shop/registShop", async (data) => {
  const res = await ShopService.create(data);
  return res.data;
});

export const updateShop = createAsyncThunk("Shop/updateShop", async (data) => {
  const res = await ShopService.update(data);
  return res.data;
});

const shopSlice = createSlice({
  name: "shop",
  initialState: initialState,
  extraReducers: {
    [findShopInfoById.pending]: (state, action) => {
      return { shopInfo: null, isLoading: true };
    },
    [findShopInfoById.fulfilled]: (state, action) => {
      return { ...state, shopInfo: action.payload, isLoading: false };
    },
    [findShopInfoById.rejected]: (state, action) => {
      return { ...state, shopInfo: action.payload, isLoading: false };
    },
    [registShop.fulfilled]: (state, action) => {
      return { ...state, shopInfo: action.payload, isLoading: false };
    },
    [updateShop.fulfilled]: (state, action) => {
      return { ...state, shopInfo: action.payload, isLoading: false };
    },
  },
});

const { reducer } = shopSlice;

export default reducer;
