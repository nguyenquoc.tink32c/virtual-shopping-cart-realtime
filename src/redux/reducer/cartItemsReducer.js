import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import CartService from "../../services/CartService";

const initialState = {
  data: null,
  isLoading: true,
};

export const createCart = createAsyncThunk("Cart/createCart", async (data) => {
  const res = await CartService.create(JSON.stringify(data));

  return res.data;
});

export const submitItem = createAsyncThunk("Carts/submitItem", async (data) => {
  const res = await CartService.submit(JSON.stringify(data));

  return res.data;
});

export const removeItem = createAsyncThunk("Carts/removeItem", async (data) => {
  const res = await CartService.removeItem(JSON.stringify(data));

  return res.data;
});

export const addItem = createAsyncThunk("Carts/removeItem", async (data) => {
  const res = await CartService.addOneItem(JSON.stringify(data));

  return res.data;
});

const cartItemsSlice = createSlice({
  name: "cartItems",
  initialState: initialState,
  extraReducers: {
    [submitItem.fulfilled]: (state, action) => {
      return {
        data: action.payload,
        isLoading: false,
      };
    },
    [removeItem.fulfilled]: (state, action) => {
      return {
        data: action.payload,
        isLoading: false,
      };
    },
    [addItem.fulfilled]: (state, action) => {
      return {
        data: action.payload,
        isLoading: false,
      };
    },
    [createCart.fulfilled]: (state, action) => {
      return {
        data: action.payload,
        isLoading: false,
      };
    },
  },
});

const { reducer } = cartItemsSlice;

export default reducer;
