import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import OrderService from "../../services/OrderService";

const initialState = {
  data: null,
  isLoading: true,
};

export const findAllOrdersByShopId = createAsyncThunk(
  "Orders/findByShopId",
  async ({ id }) => {
    const res = await OrderService.getAllOrdersByShopId(id);

    return res.data;
  }
);

export const findCartById = createAsyncThunk(
  "Orders/findCartById",
  async ({ id }) => {
    const res = await OrderService.getCartById(id);

    return res.data;
  }
);

export const updateStatus = createAsyncThunk(
  "Orders/updateStatus",
  async (data) => {
    const res = await OrderService.updateOrderStatus(data);

    return res.data;
  }
);

export const cancelStatus = createAsyncThunk(
  "Orders/cancelStatus",
  async (data) => {
    const res = await OrderService.cancelOrderStatus(data);

    return res.data;
  }
);

export const placedOrder = createAsyncThunk(
  "Order/placedOrder",
  async (data) => {
    const res = await OrderService.placedNewOrder(data);

    return res.data;
  }
);

export const getAllOrdersByCustomerId = createAsyncThunk(
  "Orders/getAllOrdersByCustomerId",
  async ({ id }) => {
    const res = await OrderService.getAllOrdersByCutomserId(id);

    return res.data;
  }
);

const orderSlice = createSlice({
  name: "order",
  initialState: initialState,
  extraReducers: {
    [findAllOrdersByShopId.fulfilled]: (state, action) => {
      return {
        data: action.payload,
        isLoading: false,
      };
    },
    [findCartById.fulfilled]: (state, action) => {
      return {
        data: action.payload,
        isLoading: false,
      };
    },
    [cancelStatus.fulfilled]: (state, action) => {
      return {
        data: action.payload,
        isLoading: false,
      };
    },
    [updateStatus.fulfilled]: (state, action) => {
      return {
        data: action.payload,
        isLoading: false,
      };
    },
    [updateStatus.fulfilled]: (state, action) => {
      return {
        data: action.payload,
        isLoading: false,
      };
    },
    [placedOrder.fulfilled]: (state, action) => {
      return {
        data: action.payload,
        isLoading: false,
      };
    },
    [getAllOrdersByCustomerId.fulfilled]: (state, action) => {
      return {
        data: action.payload,
        isLoading: false,
      };
    },
  },
});

const { reducer } = orderSlice;

export default reducer;
