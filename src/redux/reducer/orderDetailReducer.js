import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import OrderService from "../../services/OrderService";

const initialState = {
  data: null,
  isLoading: true,
};

export const findOrderById = createAsyncThunk(
  "Orders/findOrderById",
  async ({ id }) => {
    const res = await OrderService.getOrderById(id);

    return res.data;
  }
);

const orderDetailSlice = createSlice({
  name: "order",
  initialState: initialState,
  extraReducers: {
    [findOrderById.fulfilled]: (state, action) => {
      return {
        data: action.payload,
        isLoading: false,
      };
    },
  },
});

const { reducer } = orderDetailSlice;

export default reducer;
