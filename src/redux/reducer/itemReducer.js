import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import ItemService from "../../services/ItemService";

const initialState = {
  data: null,
  isLoading: true,
};

export const createItem = createAsyncThunk("item/create", async (data) => {
  const res = await ItemService.create(data);
  return res.data;
});

export const updateItem = createAsyncThunk("item/update", async (data) => {
  const res = await ItemService.update(data);
  return res.data;
});

export const deleteItem = createAsyncThunk("item/remove", async (data) => {
  const res = await ItemService.remove(data);
  return res.data;
});

export const findItemById = createAsyncThunk("item/findById", async (id) => {
  const res = await ItemService.get(id);
  return res.data;
});

const itemSlice = createSlice({
  name: "item",
  initialState,
  extraReducers: {
    [createItem.fulfilled]: (state, action) => {
      return { ...state, data: action.payload, isLoading: false };
    },
    [updateItem.fulfilled]: (state, action) => {
      return { ...state, data: action.payload, isLoading: false };
    },
    [deleteItem.fulfilled]: (state, action) => {
      return { ...state, data: action.payload, isLoading: false };
    },
    [findItemById.fulfilled]: (state, action) => {
      return { data: action.payload, isLoading: false };
    },
  },
});

const { reducer } = itemSlice;

export default reducer;
