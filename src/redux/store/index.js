import { configureStore } from "@reduxjs/toolkit";

import shopReducer from "../reducer/shopReducer";
import loginReducer from "../reducer/loginReducer";
import orderReducer from "../reducer/orderReducer";
import orderDetailReducer from "../reducer/orderDetailReducer";
import customerCartReducer from "../reducer/customerCartReducer";
import cartItemReducer from "../reducer/cartItemsReducer";
import itemReducer from "../reducer/itemReducer";

const reducer = {
  shop: shopReducer,
  login: loginReducer,
  order: orderReducer,
  orderDetail: orderDetailReducer,
  customerCart: customerCartReducer,
  cartItem: cartItemReducer,
  item: itemReducer,
};

const index = configureStore({
  reducer: reducer,
  devTools: true,
});

export default index;
